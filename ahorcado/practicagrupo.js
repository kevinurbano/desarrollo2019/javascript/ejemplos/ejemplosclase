/**
		 * colocar las palabras para jugar
		 *
		 */
		let palabras = ["casa","jardin","coche"];
		let aciertos = 0; 
		let fallos = 0;

		/**
		 * no tocar
		 * 
		 */
		let palabraEscogida = "";
		/*Función que devuelve una palabra del array de forma aleatoria*/
		function devolver(palabras){
			let palabra=""
			palabra = palabras[parseInt(Math.random()*palabras.length)];
			return palabra;
		}
		/*Funcion que dibuja los divs*/
		function dibujar(palabra,selector){
			let caja=document.querySelector(selector);

			for (let i = 0; i < palabra.length; i++) {
				caja.innerHTML += "<div>&nbsp</div>";
			}
		}
		/*funcion que comprueba las letras de cada palabra*/
		function comprobar (){
			let letra=document.querySelector("#texto").value;
			let cajas = document.querySelectorAll("#cuadros > div");
			let cajaError = document.querySelector("#errores");
			let acertar = 0;

			for (let i = 0; i < cajas.length; i++) {
				if (letra==palabraEscogida[i]) {
					cajas[i].innerHTML = letra;
					acertar=1;
					aciertos++;
				}	
			}

			// No has acertado
			if(acertar==0){
				cajaError.innerHTML += "<div>"+letra+"</div";
				errores();
			}
			else if(palabraEscogida.length==aciertos){ // Compruebo si has ganado
				setTimeout( function(){
				   location.reload();
				   alert("Ganaste");
				 }, 200) ;
			}

		}

		function errores(){
			
			let fotos=document.querySelectorAll("img");
			console.log(fotos);
			if (fallos<=3) {
				fotos[fallos].style.visibility = 'visible';
				fallos++;
			}

		}
		palabraEscogida=devolver(palabras);

		/**
		 * esto no lo ejecutes hasta que cargue toda la web
		 */

		window.addEventListener("load",function(){
			dibujar(palabraEscogida,"#cuadros");	
			document.querySelector("button").addEventListener("click",function(){
				
				comprobar();
			});

		});