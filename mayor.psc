Algoritmo Mayor
	// Introducir 3 numeros y devolver el mayor
	a <- 0
	b <- 0
	c <- 0
	Escribir "Escribe un numero"
	Leer a
	Escribir "Escribe otro numero"
	Leer b
	Escribir "Escribe el tercer numero"
	Leer c
	Si a>b Entonces
		Si a>c Entonces
			Escribir a
		SiNo
			Escribir c
		FinSi
	SiNo
		Si b>c Entonces
			Escribir b
		SiNo
			Escribir c
		FinSi
	FinSi
FinAlgoritmo

